(ns demo.pb
  (:use [penumbra opengl]
	    [cantor])
  (:require [penumbra.app :as app]
            [penumbra.text :as text])
  (:gen-class))

(defn pic []
  (material :front-and-back
            :ambient-and-diffuse [1 0.25 0.25 1])
  )

(defn init [state]
  (print state)
  (app/title! "Gears")
  (app/vsync! false)
  (enable :depth-test)
  (enable :lighting)
  (enable :light0)
  (shade-model :flat)
  state)

(defn display [[delta time] start]
  (scale (/ 2 10) (/ 2 20) 1)
  (rotate (* (+ 20.) (rem time 360)) 0 0 1)
  (text/write-to-screen (format "%d FPS" (int (/ 1 delta))) 0 0)
  (pic)
  (app/repaint!))

(defmacro proxy [func]
  `#(apply ~func %&))

(defn start []
  (app/start
   {:display (proxy display) :init init}
   {}))
